#!/bin/env bash
# Script to run as cloud init after create a VM
# customized for AWS Docker-AMI

set -e

# Install Docker-Compose
yum install -y -q wget
wget -q -O /usr/local/bin/docker-compose https://github.com/docker/compose/releases/download/1.23.2/docker-compose-Linux-x86_64
chmod +x /usr/local/bin/docker-compose

mkdir /home/ec2-user/workshop
cd /home/ec2-user/workshop

# Create .env for docker-compose
cat << EOS > .env
DBNAME=${DBNAME}
PGHOST=${PGHOST}
PGPASSWORT=${PGPASSWORT}
PGUSER=root
PROJECT_DOMAIN=${PROJECT_DOMAIN}
EOS

# Create docker-compose-yml
echo '${docker_compose_yml}' > docker-compose.yml


# Pull Docker-Images and Start the Docker-Containers
#sudo -u ec2-user docker login -u gitlab-ci-token -p 'xxxx' registry.gitlab.com
sudo -u ec2-user /usr/local/bin/docker-compose pull
sudo -u ec2-user /usr/local/bin/docker-compose up -d

chown -R ec2-user /home/ec2-user/workshop
