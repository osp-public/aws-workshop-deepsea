variable "user_id" {
  description = "Uniq User id in the context of the workshop"
  type = "string"
}


variable "ec2_image" {
  description = "Image-Pattern to find AMI-Image"
  default     = "amzn2-ami-ecs-hvm*"
  type        = "string"
}


locals {
  ec2_instance_types = {
    "default" = "t2.small"
    "prod" = "t2.medium"
  }


  # DB-Param
  allow_major_version_upgrades = {
    "default" = "true"
    "prod" = "false"
  }

  # DB-Param
  backup_retention_periods = {
    "default" = 2
    "prod" = 14
  }

  # logs-Param
  logs_backup_retention_periods = {
    "default" = 14
    "prod" = 30
  }


  ec2_instance_type            = "${lookup(local.ec2_instance_types,terraform.workspace)}"
  backup_retention_period      = "${lookup(local.backup_retention_periods,terraform.workspace)}"
  allow_major_version_upgrade  = "${lookup(local.allow_major_version_upgrades,terraform.workspace)}"
  db_name                      = "workshopdb_${terraform.workspace}_${var.user_id}"
  logs_backup_retention_period = "${lookup(local.logs_backup_retention_periods,terraform.workspace)}"

  # ARN of the last DB-Snapshots as base for the new DB-Instance
  #db_snapshot = "arn:aws:rds:eu-central-1:512241874122:snapshot:copy-from-prod-2018-05-07"
}


variable "workshop_domain" {
  type    = "string"
  default = "workshop.osp-dev.de"
}

variable "route53_osp_dev_de_zoneid" {
  type    = "string"
  default = "Z3GBNU3TT7CSZB"  # Zone-ID of osp-dev.de in route53
}


