
# security group for crispytrain backend
resource "aws_security_group" "workshop_sec" {
  name        = "workshop-sec-${terraform.workspace}-${var.user_id}"
  description = "security group that allows inbound for web&ssh and outbound traffic from all instances in the VPC"

  #vpc_id = "${aws_vpc.default.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name = "aws-workshop"
  }
}


data "aws_ami" "ami_image" {
  most_recent = true

  filter {
    name   = "name"
    values = ["${var.ec2_image}"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["591542846629"] # Amazon
}

# Template for shell-script with install docker-compose, create docker-compose.yml,
# pull images and up the containers
data "template_file" "app_init_tpl" {
  template = "${file("app-init.tpl.sh")}"

  vars {
    DBNAME         = "${local.db_name}"
    PGHOST         = "${aws_db_instance.workshop_db.endpoint}"
    PGPASSWORT     = "${random_string.db_password.result}"
    PROJECT_DOMAIN = "${var.user_id}.${terraform.workspace}-compute.${var.workshop_domain}"
    docker_compose_yml = "${file("docker-compose.yml")}"
  }
}


resource "aws_instance" "simple_compute" {
  ami                         = "${data.aws_ami.ami_image.id}"
  instance_type               = "${local.ec2_instance_type}"
  key_name                    = "${aws_key_pair.workshop_ssh.key_name}"
  associate_public_ip_address = true

  tags {
    Name = "aws workshop-sample02-${var.user_id}"
  }

  security_groups = ["default", "${aws_security_group.workshop_sec.name}"]
  user_data       = "${data.template_file.app_init_tpl.rendered}"
}


resource "aws_route53_record" "backend_dns" {
  zone_id = "${var.route53_osp_dev_de_zoneid}"           # Id der Zone "osp-dev.de"
  name    = "*.${var.user_id}.${terraform.workspace}-compute.${var.workshop_domain}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.simple_compute.public_ip}"]
}



output "public_ip" {
  value = "${aws_instance.simple_compute.public_ip}"
}
