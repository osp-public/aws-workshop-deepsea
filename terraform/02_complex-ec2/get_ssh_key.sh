#!/usr/bin/env bash

set -e

if [ -z "$1" ]; then
  echo "Use:  $0  «user-id»"
  exit 1
fi

workspace=$(terraform workspace show)
user_id=$1

AWS_PARAM_PATH="/workshop/$user_id/$workspace"
SSH_FILE=workshop-$user_id-$workspace

script_dir=`dirname "$0"`
# goto dir of script
cd ${script_dir}

aws ssm get-parameter --name "$AWS_PARAM_PATH/ssh_key_pub" --query Parameter.Value --output text > $SSH_FILE.pub
aws ssm get-parameter --name "$AWS_PARAM_PATH/ssh_key_pem" --with-decryption --query Parameter.Value --output text > $SSH_FILE.pem

chmod 0600 *.pem

echo "SSH-Keys copied from AWS Paramater Store:  $SSH_FILE.pub  & $SSH_FILE.pem"

