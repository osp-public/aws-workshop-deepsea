resource "random_string" "db_password" {
  length  = 16
  special = false
}

resource "aws_db_instance" "workshop_db" {
  allocated_storage           = 10
  allow_major_version_upgrade = "${local.allow_major_version_upgrade}"
  auto_minor_version_upgrade  = true
  backup_retention_period     = "${local.backup_retention_period}"
  storage_type                = "gp2"
  engine                      = "postgres"
  engine_version              = "9.6.11"
  instance_class              = "db.t2.small"    # see https://docs.aws.amazon.com/de_de/AmazonRDS/latest/UserGuide/Concepts.DBInstanceClass.html
  publicly_accessible         = false
  storage_encrypted           = true                                   # you should always do this
  skip_final_snapshot         = true
  name                        = "${local.db_name}"
  username                    = "root"
  password                    = "${random_string.db_password.result}"
  #snapshot_identifier         = "${local.db_snapshot}"

  tags = {
    Application = "aws_workshop"
    Stage       = "${terraform.workspace}"
  }
}

resource "aws_ssm_parameter" "pghost_ssm" {
  name  = "/workshop/${var.user_id}/${terraform.workspace}/pghost"
  type  = "String"
  value = "${aws_db_instance.workshop_db.endpoint}"
}

resource "aws_ssm_parameter" "pg_db_id_ssm" {
  name  = "/workshop/${var.user_id}/${terraform.workspace}/pg_db_id"
  type  = "String"
  value = "${aws_db_instance.workshop_db.id}"
}

resource "aws_ssm_parameter" "pgpass_ssm" {
  name  = "/workshop/${var.user_id}/${terraform.workspace}/pgpassword"
  type  = "SecureString"
  value = "${random_string.db_password.result}"
}
