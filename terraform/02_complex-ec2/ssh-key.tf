
resource "tls_private_key" "workshop_key" {
  algorithm   = "RSA"
}

resource "aws_key_pair" "workshop_ssh" {
  key_name   = "workshop-key-${var.user_id}"
  public_key = "${tls_private_key.workshop_key.public_key_openssh}"
}

resource "aws_ssm_parameter" "ssh_private" {
  name = "/workshop/${var.user_id}/${terraform.workspace}/ssh_key_pem"
  type = "SecureString"
  value = "${tls_private_key.workshop_key.private_key_pem}"
  description = "The PRIVATE ssh-key created by terraform. This key can be downloaded via 'get_ssh_key.sh' and used to login."
}

resource "aws_ssm_parameter" "ssh_public" {
  name = "/workshop/${var.user_id}/${terraform.workspace}/ssh_key_pub"
  type = "String"
  value = "${tls_private_key.workshop_key.public_key_openssh}"
  description = "The PUBLIC ssh-key created by terraform. This key can be downloaded via 'get_ssh_key.sh' and used to login."
}

output "workshop_private_key" {
  value = "${tls_private_key.workshop_key.private_key_pem}"
}

output "workshop_public_key" {
  value = "${tls_private_key.workshop_key.public_key_openssh}"
}
