#!/usr/bin/env bash
set -e

if [ -z "$1" ]; then
  echo "Use:  $0  «user-id»"
  exit 1
fi

workspace=$(terraform workspace show)
user_id=$1
SSH_FILE=workshop-$user_id-$workspace.pem
WORKSHOP_HOST="srv.${user_id}.${workspace}-compute.workshop.osp-dev.de"

script_dir=`dirname "$0"`
# goto dir of script
cd ${script_dir}

ssh -i ./$SSH_FILE -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ec2-user@$WORKSHOP_HOST -L 9080:localhost:9080

