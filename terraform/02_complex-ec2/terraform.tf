terraform {
  backend "s3" {
    bucket         = "osp-stb-terraformstates"
    key            = "aws-workshop-bl-02"
    region         = "eu-central-1"
    dynamodb_table = "terraform-state-lock-dynamo"
  }
}

provider "aws" {
  region = "eu-central-1"

  # AccesKey and secret must set in OS ENV!
}

