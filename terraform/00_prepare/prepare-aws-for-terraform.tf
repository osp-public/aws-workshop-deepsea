provider "aws" {
  region = "eu-central-1"

  # AccesKey and secret must set in OS ENV!
}



resource "aws_s3_bucket" "terraform-state-storage-s3" {
  bucket = "osp-stb-terraformstates"

  versioning {
    enabled = false
  }

  lifecycle {
    prevent_destroy = true
  }

  tags {
    Name = "S3 Remote Terraform State Store"
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
      }
    }
  }
}

resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name = "terraform-state-lock-dynamo"
  hash_key = "LockID"
  read_capacity = 10
  write_capacity = 10

  attribute {
    name = "LockID"
    type = "S"
  }

  tags {
    Name = "DynamoDB Terraform State Lock Table"
  }
}
