
resource "aws_key_pair" "workshop_ssh" {
  key_name   = "workshop-key-${var.user_id}"
  public_key = "${file("${var.ssh_key_file_pub}")}"
}

# security group for workshop
resource "aws_security_group" "workshop_sec" {
  name        = "workshop-sec-${var.user_id}"
  description = "security group that allows inbound for web&ssh and outbound traffic from all instances in the VPC"

  #vpc_id = "${aws_vpc.default.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name = "aws-workshop"
  }
}


data "aws_ami" "ami_image" {
  most_recent = true

  filter {
    name   = "name"
    values = ["${var.ec2_image}"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["591542846629"] # Amazon
}



resource "aws_instance" "simple_compute" {
  ami                         = "${data.aws_ami.ami_image.id}"
  instance_type               = "${var.ec2_instance_type}"
  key_name                    = "${aws_key_pair.workshop_ssh.key_name}"
  associate_public_ip_address = true

  tags {
    Name = "aws workshop-${var.user_id}"
  }

  security_groups = ["${aws_security_group.workshop_sec.name}"]
}


output "public_ip" {
  value = "${aws_instance.simple_compute.public_ip}"
}
