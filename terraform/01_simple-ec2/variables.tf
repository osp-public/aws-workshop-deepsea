variable "user_id" {
  description = "Uniq User id in the context of the workshop"
  type = "string"
}

variable "ec2_instance_type" {
  description = "EC2 sizing"
  default     = "t3.nano"
  type        = "string"
}

variable "ec2_image" {
  description = "Image-Pattern to find AMI-Image"
  default     = "amzn2-ami-ecs-hvm*"
  type        = "string"
}

variable "ssh_key_file_pub" {
  default = "~/.ssh/id_rsa.pub"
}
